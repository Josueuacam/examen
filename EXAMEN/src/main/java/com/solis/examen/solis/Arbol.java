/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solis.examen.solis;

/**
 *
 * @author pc
 */
public class Arbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        
        //JOSUE SOLIS
        ArbolBinario miArbol = new ArbolBinario();
        miArbol.agregarNodo(79, "J");
        miArbol.agregarNodo(83, "O");
        miArbol.agregarNodo(76, "S");
        miArbol.agregarNodo(63, "U");
        miArbol.agregarNodo(78, "E");
        miArbol.agregarNodo(63, "L");
        miArbol.agregarNodo(73, "I");

        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        
        
    }
    
}